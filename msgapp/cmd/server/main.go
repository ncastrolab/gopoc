package main

import (
	"gitlab.ncastrolab.gopoc.msgapp/pkg/consumer"
	"gitlab.ncastrolab.gopoc.msgapp/pkg/server"
)

func main() {
	server.ConsumerApp.Initialize()
	//uCollection := server.ConsumerApp.Db.GetCollection()
	//if uCollection
	_, channelRabbitMQ := consumer.BuildCnx()
	consumer.StartConsuming(channelRabbitMQ)
}