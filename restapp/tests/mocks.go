package tests

type TestBroker struct {
}

func (t *TestBroker) BuildCnx() {

}

func (t *TestBroker) CloseCnx() error {
	return nil
}

func (t *TestBroker) Publish(toPublish map[string]string) error {
	return nil
}
