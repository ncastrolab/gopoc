module gitlab.ncastrolab.gopoc.restapp

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.5.3
)
