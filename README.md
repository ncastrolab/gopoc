## GoPoc

Esta mini poc hecha con Go apunta a mostrar un servicio Rest que expone 2 endpoint, uno para crear y otro para obtener
usuarios. Al crear un usuario se envia un msg a una cola de rabbit para que este usuario sea procesado. Por otro lado
hay un servicio rabbitmq escuchando en una Queue que al recibir un Id de un usuario lo que hace es marcarlo como "
completado" en la db. Todo se desarrollo usando Tdd orientado a test de integración.

#### Objetivo

Ver como se comporta el lenguaje y conocerlo de manera general. Poder no solamente entender el modelado y la composicion
de informacion sino tambien la conexion entre servicios.

#### Faltantes

* Faltaría que el mock de la conexión amqp sirva para validar como y cuantas veces se llamó al publish
* El servicio del usuario debería ser genérico y debería servir para cualquier modelo
* No hay implementada una inyección de dependencias
* Posiblemente hubiese sido bueno agregar un servicio para obtener todos los usuarios creados, pero ahi ya sería
  necesaria una paginacion y siento que exede el scope del ejercicio
* Controlar con un script que rabbit ya este levantado para que no sea necesario levantarlo separado
* Me falta indagar en el tema logger. Me cuesta entender como hacer para que aparezcan los logs en el stdout


#### kubernetes

la configuracion de kubernetes es solo para el contenedor `restapp` y la idea es tener mongo y rabbit levantados con docker-compose. En una terminal hay que tener corriendo `docker-compose up mongodb_container message-broker` y desde los pods se va a conectar a mongo y rabbit en la red local.