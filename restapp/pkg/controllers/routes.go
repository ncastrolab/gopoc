package controllers

import (
	"github.com/gin-gonic/gin"
)

type AppExternal struct {
	Router *gin.Engine
}

func (external *AppExternal) SetupRouter() {
	r := gin.Default()
	r.GET("/", HelloController)
	r.GET("/status", StatusController)
	r.GET("/users/:id", GetUserController)
	r.POST("/users", CreateUserController)
	external.Router = r
}