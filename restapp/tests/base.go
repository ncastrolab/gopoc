package tests

import (
	"encoding/json"
	"fmt"
	"gitlab.ncastrolab.gopoc.restapp/pkg/server"
	"log"
	"time"
)

func setup() server.Application {
	app := server.Application{}
	app.Init(&TestBroker{})
	return app
}

func teardown(app server.Application) {
	err := app.Db.FlushDB()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\033[1;36m%s\033[0m", "> Teardown completed without error")
	fmt.Printf("\n")
}

type UserJson struct {
	ID        string
	CreatedAt time.Time
	UpdatedAt time.Time
	Name      string
	LastName  string
	Sex       json.Number
	Completed bool
}
