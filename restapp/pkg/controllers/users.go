package controllers

import (
	"gitlab.ncastrolab.gopoc.restapp/pkg/amqp"
	"gitlab.ncastrolab.gopoc.restapp/pkg/services"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetUserController(c *gin.Context) {
	objID, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	user, err := services.BaseService.UserByID(objID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	c.JSON(200, user)
}

func CreateUserController(c *gin.Context) {
	var newU services.User

	err := c.ShouldBindJSON(&newU)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	inserted, err := services.BaseService.CreateUser(&newU)
	body := make(map[string]string)
	body["id"] = inserted.InsertedID.(primitive.ObjectID).Hex()
	err = amqp.PublicBroker.Publish(body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": inserted.InsertedID.(primitive.ObjectID).Hex()})
}
