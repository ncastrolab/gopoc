

# registry create

`k3d registry create gopoc.registry --port `

# add to hosts

`127.0.0.1 k3d-gopoc.registry`

# build image

`docker build -t gopoc-restapp .`

# tag image

`docker tag gopoc-restapp:latest k3d-gopoc.registry:49900/gopoc-restapp:v0.1.1`


# push image

`docker push k3d-gopoc.registry:49900/gopoc-restapp:v0.1.1`

# create cluster

`k3d cluster create gopoc --registry-use k3d-gopoc.registry:49900 --port 9900:80@loadbalancer`

# apply deployment

`kubectl apply -f deployments/restapp-deployment-k3d.yml`

# create service for pods

`kubectl create service clusterip gopoc-restapp --tcp=9092:9092`

# apply ingress

`kubectl apply -f ingress/ingress.yaml`