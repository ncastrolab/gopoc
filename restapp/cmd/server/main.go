package main

import (
	"gitlab.ncastrolab.gopoc.restapp/pkg/amqp"
	"gitlab.ncastrolab.gopoc.restapp/pkg/server"
)

func main() {
	app := server.GlobalApp
	broker := &amqp.Broker{}
	app.Init(broker)
	err := app.External.Router.Run(":9092")
	if err != nil {
		return 
	}
}