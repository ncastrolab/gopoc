module gitlab.ncastrolab.gopoc.msgapp

go 1.16

require (
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/klauspost/compress v1.12.2 // indirect
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.5.3
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9 // indirect
	golang.org/x/text v0.3.6 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
