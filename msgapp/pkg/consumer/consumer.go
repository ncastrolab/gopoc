package consumer

import (
	"encoding/json"
	"gitlab.ncastrolab.gopoc.msgapp/pkg/repository"
	"gitlab.ncastrolab.gopoc.msgapp/pkg/server"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"os"

	"github.com/streadway/amqp"
)

func BuildCnx() (*amqp.Connection, *amqp.Channel) {
	amqpServerURL := os.Getenv("AMQP_SERVER_URL")

	connectRabbitMQ, err := amqp.Dial(amqpServerURL)
	if err != nil {
		panic(err)
	}

	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		panic(err)
	}
	return connectRabbitMQ, channelRabbitMQ
}

func StartConsuming(channelRabbitMQ *amqp.Channel) {

	messages, err := channelRabbitMQ.Consume(
		"completeUsers", // queue name
		"",              // consumer
		true,            // auto-ack
		false,           // exclusive
		false,           // no local
		false,           // no wait
		nil,             // arguments
	)
	if err != nil {
		log.Println(err)
	}

	log.Println("Successfully connected to RabbitMQ")
	log.Println("Waiting for messages")

	forever := make(chan bool)

	go func(dbCnx *repository.DbCnx) {
		for message := range messages {
			body := message.Body
			m := make(map[string]string)
			err := json.Unmarshal(body, &m)
			if err != nil {
				log.Println("Error with message")
				log.Println(err)
				return
			}
			objID, _ := primitive.ObjectIDFromHex(m["id"])

			completed, err := dbCnx.MarkCompleted(objID)
			if err != nil {
				log.Println("Error updating element")
				log.Println(err)
				return
			}
			log.Println("Successfully updated element")
			log.Println(completed)
		}
	}(server.ConsumerApp.Db)

	<-forever
}
