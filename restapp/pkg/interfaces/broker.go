package interfaces

type MsgConnector interface {
	BuildCnx()
	CloseCnx() error
	Publish(toPublish map[string]string) error
}