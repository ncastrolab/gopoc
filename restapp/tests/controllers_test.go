package tests

import (
	"bytes"
	"encoding/json"
	"gitlab.ncastrolab.gopoc.restapp/pkg/services"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func TestStatusRoute(t *testing.T) {
	app := setup()
	defer teardown(app)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/status", nil)
	app.External.Router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "{\"message\":\"Ok\"}", w.Body.String())
}

func TestGetUserFromRest(t *testing.T) {
	cmd := setup()
	defer teardown(cmd)
	userToCreate := &services.User{Name: "Nazareno", LastName: "Castro", Sex: 0}
	result, _ := services.BaseService.CreateUser(userToCreate)

	w := httptest.NewRecorder()

	req, _ := http.NewRequest("GET", "/users/"+result.InsertedID.(primitive.ObjectID).Hex(), nil)
	// Convert the JSON response to a map
	cmd.External.Router.ServeHTTP(w, req)
	response := &UserJson{}
	err := json.Unmarshal(w.Body.Bytes(), &response)
	assert.Nil(t, err)
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, userToCreate.CreatedAt, response.CreatedAt)
	//	TODO: Falta chequear por mas cosas
}

func TestCreateUserFromRest(t *testing.T) {
	app := setup()
	defer teardown(app)

	w := httptest.NewRecorder()

	var jsonStr = []byte("{\"name\":\"nacho_test\",\"sex\":2,\"lastname\":\"castro\"}")
	req, _ := http.NewRequest("POST", "/users", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	app.External.Router.ServeHTTP(w, req)

	var response map[string]string
	err := json.Unmarshal(w.Body.Bytes(), &response)
	if err != nil {
		return
	}
	objID, _ := primitive.ObjectIDFromHex(response["id"])
	userFromDb, _ := services.BaseService.UserByID(objID)

	assert.Equal(t, 201, w.Code)
	assert.Equal(t, userFromDb.Sex, 2)
	assert.Equal(t, userFromDb.Name, "nacho_test")
	assert.Equal(t, userFromDb.LastName, "castro")
}
