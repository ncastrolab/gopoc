package repository

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
)

var clientInstanceError error

type DbCnx struct {
	clientInstance *mongo.Client
}

var (
	CONNECCTIONSTRING = os.Getenv("MONGO_URL")
	DB                = os.Getenv("MONGO_DB_NAME")
	USERS             = "col_users"
)

func (db *DbCnx) InitDb() (*mongo.Client, error) {
	credential := options.Credential{
		Username: "root",
		Password: "rootpassword",
	}

	clientOptions := options.Client().ApplyURI(CONNECCTIONSTRING).SetAuth(credential)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		clientInstanceError = err
		panic(err)
	}
	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		clientInstanceError = err
		panic(err)
	}
	db.clientInstance = client

	return db.clientInstance, clientInstanceError
}

func (db *DbCnx) GetCollection() *mongo.Collection {
	return db.clientInstance.Database(DB).Collection(USERS)
}

func (db *DbCnx) FlushDB() error {
	collection := db.GetCollection()
	err := collection.Drop(context.TODO())
	return err
}
