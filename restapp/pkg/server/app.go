package server

import (
	"gitlab.ncastrolab.gopoc.restapp/pkg/controllers"
	"gitlab.ncastrolab.gopoc.restapp/pkg/interfaces"
	"gitlab.ncastrolab.gopoc.restapp/pkg/repository"
	"gitlab.ncastrolab.gopoc.restapp/pkg/services"
)

type Application struct {
	Db          *repository.DbCnx
	External    controllers.AppExternal
	MsgBroker   interfaces.MsgConnector
}

func (app *Application) Init(messenger interfaces.MsgConnector) {
	app.Db = &repository.DbCnx{}
	app.External = controllers.AppExternal{}
	services.BaseService.SetDb(app.Db)
	_, err := app.Db.InitDb()
	if err != nil {
		panic(err)
	}
	app.External.SetupRouter()
	app.MsgBroker = messenger
	app.MsgBroker.BuildCnx()
}

var GlobalApp = Application{}
