package server

import (
	"gitlab.ncastrolab.gopoc.msgapp/pkg/repository"
)

type Consumer struct {
	Db *repository.DbCnx
}

func (c *Consumer) Initialize() {
	c.Db = &repository.DbCnx{}
	_, err := c.Db.InitDb()
	if err != nil {
		panic(err)
	}
}

var ConsumerApp = &Consumer{}