package repository

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
)

var clientInstanceError error

type DbCnx struct {
	clientInstance *mongo.Client
}

var (
	CONNECCTIONSTRING = os.Getenv("MONGO_URL")
	DB                = os.Getenv("MONGO_DB_NAME")
	USERS             = "col_users"
)

func (db *DbCnx) InitDb() (*mongo.Client, error) {
	credential := options.Credential{
		Username: "root",
		Password: "rootpassword",
	}

	clientOptions := options.Client().ApplyURI(CONNECCTIONSTRING).SetAuth(credential)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		clientInstanceError = err
		panic(err)
	}
	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		clientInstanceError = err
		panic(err)
	}
	db.clientInstance = client

	return db.clientInstance, clientInstanceError
}

func (db *DbCnx) GetCollection() *mongo.Collection {
	return db.clientInstance.Database(DB).Collection(USERS)
}

func (db *DbCnx) FlushDB() error {
	collection := db.GetCollection()
	err := collection.Drop(context.TODO())
	return err
}

func (db *DbCnx) MarkCompleted(id primitive.ObjectID) (*mongo.UpdateResult, error) {
	filter := bson.D{primitive.E{Key: "_id", Value: id}}

	err := db.clientInstance.Ping(context.TODO(), nil)
	if err != nil {
		panic("No anda la db")
	}

	updater := bson.D{primitive.E{Key: "$set", Value: bson.D{
		primitive.E{Key: "completed", Value: true},
	}}}
	collection := db.GetCollection()

	return collection.UpdateOne(context.TODO(), filter, updater)
}

func (db *DbCnx) GetById(id primitive.ObjectID) (*bson.D, error) {
	collection := db.GetCollection()
	filter := bson.D{primitive.E{Key: "_id", Value: id}}
	JSONData := &bson.D{}
	err := collection.FindOne(context.TODO(), filter).Decode(&JSONData)
	if err != nil {
		return nil, err
	}
	return JSONData, err
}
