package tests

import (
	"gitlab.ncastrolab.gopoc.restapp/pkg/services"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDbUp(t *testing.T) {
	app := setup()
	defer teardown(app)
	collection := app.Db.GetCollection()
	assert.NotNil(t, collection)
}

func TestSaveUser(t *testing.T) {
	app := setup()
	defer teardown(app)
	userToCreate := &services.User{Name: "Nazareno", LastName: "Castro", Sex: 0}
	result, err := services.BaseService.CreateUser(userToCreate)
	assert.Empty(t, err)
	assert.Equal(t, userToCreate.ID, result.InsertedID)
}

func TestSaveUserAndRetrieveById(t *testing.T) {
	app := setup()
	defer teardown(app)
	userToCreate := &services.User{Name: "Nazareno", LastName: "Castro", Sex: 0}
	_, err := services.BaseService.CreateUser(userToCreate)
	assert.Empty(t, err)
	userFromDb, err := services.BaseService.UserByID(userToCreate.ID)
	assert.Empty(t, err)
	assert.Equal(t, *userToCreate, userFromDb)
}

func TestSaveManyUserAndCount(t *testing.T) {
	app := setup()
	defer teardown(app)
	var usersList []*services.User

	for i := 1; i <= 10; i++ {
		usersList = append(usersList, &services.User{Name: "Nazareno", LastName: "Castro", Sex: 0})
	}

	_, err := services.BaseService.CreateUserMany(usersList)
	assert.Empty(t, err)
	usersAmount, err := services.BaseService.CountAllUsers()
	assert.Empty(t, err)
	assert.Equal(t, int64(10), usersAmount)
}
