package amqp

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"gitlab.ncastrolab.gopoc.restapp/pkg/interfaces"
	"os"
)

type Broker struct {
	AmqpCnx     *amqp.Connection
	AmqpChannel *amqp.Channel
}

func (b *Broker) BuildCnx() {
	amqpServerURL := os.Getenv("AMQP_SERVER_URL")

	connectRabbitMQ, err := amqp.Dial(amqpServerURL)
	if err != nil {
		panic(err)
	}

	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		panic(err)
	}

	_, err = channelRabbitMQ.QueueDeclare(
		"completeUsers",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		panic(err)
	}
	b.AmqpCnx = connectRabbitMQ
	b.AmqpChannel = channelRabbitMQ
	PublicBroker = b
}

func (b *Broker) CloseCnx() error {
	channel := b.AmqpChannel
	cnx := b.AmqpCnx
	err := channel.Close()
	if err != nil {
		return err
	}
	err = cnx.Close()
	if err != nil {
		return err
	}
	return err
}

func (b *Broker) Publish(toPublish map[string]string) error {
	marshal, err := json.Marshal(toPublish)
	if err != nil {
		return err
	}

	channel := b.AmqpChannel
	message := amqp.Publishing{
		ContentType: "text/plain",
		Body:        marshal,
	}

	if err := channel.Publish(
		"",
		"QueueService1",
		false,
		false,
		message,
	); err != nil {
		return err
	}

	return nil
}

var PublicBroker interfaces.MsgConnector
