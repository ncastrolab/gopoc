package services

import (
	"context"
	"gitlab.ncastrolab.gopoc.restapp/pkg/repository"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type User struct {
	ID        primitive.ObjectID `bson:"_id, omitempty" `
	CreatedAt time.Time          `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at" json:"updated_at"`
	Name      string             `bson:"name" json:"name" binding:"required"`
	LastName  string             `bson:"last_name" json:"lastname" binding:"required"`
	Sex       int                `bson:"sex" json:"sex" binding:"required"`
	Completed bool               `bson:"completed, omitempty" json:"completed"`
}

type UsersService struct {
	db *repository.DbCnx
}

func (sv *UsersService) SetDb(db *repository.DbCnx) {
	sv.db = db
}

func (sv *UsersService) CreateUser(user *User) (*mongo.InsertOneResult, error) {
	user.ID = primitive.NewObjectID()
	coll := sv.db.GetCollection()
	return coll.InsertOne(context.TODO(), user)
}

func (sv *UsersService) UserByID(id primitive.ObjectID) (User, error) {
	var result User
	collection := sv.db.GetCollection()
	filter := bson.D{primitive.E{Key: "_id", Value: id}}
	err := collection.FindOne(context.TODO(), filter).Decode(&result)
	return result, err
}

func (sv *UsersService) CreateUserMany(list []*User) (*mongo.InsertManyResult, error) {
	//Map struct slice to interface slice as InsertMany accepts interface slice as parameter
	insertableList := make([]interface{}, len(list))
	for i, v := range list {
		v.ID = primitive.NewObjectID()
		insertableList[i] = v
	}
	collection := sv.db.GetCollection()

	//Perform InsertMany operation & validate against the error.
	return collection.InsertMany(context.TODO(), insertableList)

}

func (sv *UsersService) GetAllUsers() ([]User, error) {
	var users []User

	collection := sv.db.GetCollection()
	cur, findError := collection.Find(context.TODO(), bson.M{})
	if findError != nil {
		return users, findError
	}

	for cur.Next(context.TODO()) {
		t := User{}
		err := cur.Decode(&t)
		if err != nil {
			return users, err
		}
		users = append(users, t)
	}
	err := cur.Close(context.TODO())
	if err != nil {
		return nil, err
	}
	return users, mongo.ErrNoDocuments
}

func (sv *UsersService) CountAllUsers() (int64, error) {
	collection := sv.db.GetCollection()
	return collection.CountDocuments(context.TODO(), bson.M{})
}

var BaseService = &UsersService{}
