package tests

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.ncastrolab.gopoc.msgapp/pkg/server"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"testing"
)

func setup() *server.Consumer {
	server.ConsumerApp.Initialize()
	return server.ConsumerApp
}

func teardown(app *server.Consumer) {
	err := app.Db.FlushDB()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\033[1;36m%s\033[0m", "> Teardown completed without error")
	fmt.Printf("\n")
}

func TestUpdateValueById(t *testing.T) {
	app := setup()
	defer teardown(app)

	result, err := app.Db.GetCollection().InsertOne(context.TODO(), bson.D{
		{Key: "author", Value: "NicRaboy"},
	})
	assert.Nil(t, err)

	app.Db.MarkCompleted(result.InsertedID.(primitive.ObjectID))

	resultMap, err := app.Db.GetById(result.InsertedID.(primitive.ObjectID))
	assert.Nil(t, err)
	log.Println(resultMap)
	assert.Equal(t, true, resultMap.Map()["completed"])
	assert.Equal(t, "NicRaboy", resultMap.Map()["author"])
}
